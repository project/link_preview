INTRODUCTION
-------------
This modules adds new field type which stores url, title and content of that url. Providing formatter and widget for it.
Special feature is in field widget where we use Goutte library which is "a simple PHP Web Scraper" 
so we fetch data from remote urls and auto fill title and content of this field.

REQUIREMENTS
------------
Using https://github.com/FriendsOfPHP/Goutte library to fetch remote data, included in modules composer.json file

INSTALLATION
-------------
Enable module, add it as your basefield in your custom entity as link_preview and set its widget to link_preview_default 
and formatter to link_preview or use it through UI and add it as field on any entity

CONFIGURATION
-------------
Configure over UI like any other field, when adding as basefild to custom entity put like this

    $fields['links'] = BaseFieldDefinition::create('link_preview')
      ->setLabel(t('Links'))
      ->setDescription(t('Links with notes'))
      ->setSettings([
        'uri_length' => 2048,
        'title_length' => 255,
      ])
      ->setDisplayOptions('form', array(
        'type' => 'link_preview_default',
        'weight' => -20,
        'settings' => array(
          'display_label' => TRUE,
        ),
        ))
      ->setDisplayOptions('view', array(
          'label' => 'above',
          'type' => 'link_preview',
          'weight' => -20,
        ))
      ->setDisplayConfigurable('view', TRUE)        
      ->setDisplayConfigurable('form', TRUE);

NOTES
----------------------------
Logic for fetching content from URL's will not always be able to fetch content, 
it is trying to make best guess where content is and extract it, over time
it will be improved.

TODO 
Fetch all <p> from content links that have multiple <div> elements like
https://air.jordan.com/card/training-to-win-like-mike/



