<?php

namespace Drupal\link_preview\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\Core\Url;
use Drupal\link_preview\LinkPreviewItemInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'link_preview' field type.
 *
 * @FieldType(
 *   id = "link_preview",
 *   label = @Translation("Link Preview"),
 *   description = @Translation("Stores a URL string, optional varchar link text, optional preview value of link content, and optional blob of attributes to assemble a link."),
 *   default_widget = "link_preview_default",
 *   default_formatter = "link_preview",
 *   constraints = {"LinkType" = {}, "LinkAccess" = {}, "LinkExternalProtocols" = {}, "LinkNotExistingInternal" = {}}
 * )
 */
class LinkPreviewItem extends FieldItemBase implements LinkPreviewItemInterface {


  public static function defaultStorageSettings() {
    return [
      'uri_length' => 2048,
      'title_length' => 255,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = [];

    $element['uri_length'] = [
      '#type' => 'number',
      '#title' => t('Maximum uri length'),
      '#default_value' => $this->getSetting('uri_length'),
      '#required' => TRUE,
      '#description' => t('The maximum length of the uri field in characters. This is varchar type and can have maximum row size of 65,535 bytes, which is shared among all columns in table'),
      '#min' => 2048,
      '#disabled' => $has_data,
    ];

    $element['title_length'] = [
      '#type' => 'number',
      '#title' => t('Maximum title length'),
      '#default_value' => $this->getSetting('title_length'),
      '#required' => TRUE,
      '#description' => t('The maximum length of the title field in characters. This is varchar type and can have maximum row size of 65,535 bytes, which is shared among all columns in table'),
      '#min' => 2048,
      '#min' => 255,
      '#disabled' => $has_data,
    ];

    $element += parent::storageSettingsForm($form, $form_state, $has_data);

    return $element;
  }

  /**
   * {@inheritdoc}
   * 
   */
  public static function defaultFieldSettings() {
    return [
      'title' => DRUPAL_OPTIONAL,
      'link_type' => LinkPreviewItemInterface::LINK_GENERIC,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['uri'] = DataDefinition::create('uri')
      ->setLabel(t('URI'));

    $properties['title'] = DataDefinition::create('string')
      ->setLabel(t('Link text'));

    $properties['uri_content'] = DataDefinition::create('string')
      ->setLabel(t('Text'));

    $properties['options'] = MapDataDefinition::create()
      ->setLabel(t('Options'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'uri' => [
          'description' => 'The URI of the link.',
          'type' => 'varchar',
          'length' =>  $field_definition->getSetting('uri_length'),
        ],
        'title' => [
          'description' => 'The link text.',
          'type' => 'varchar',
          'length' =>  $field_definition->getSetting('title_length'),
        ],
        'uri_content' => [
          'description' => 'The content of uri.',
          'type' => 'text',
          'size' => 'big',
        ],
        'options' => [
          'description' => 'Serialized array of options for the link.',
          'type' => 'blob',
          'size' => 'big',
          'serialize' => TRUE,
        ],
      ],
      'indexes' => [
        'uri' => [['uri', 30]],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('uri')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function isExternal() {
    return $this->getUrl()->isExternal();
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'uri';
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return Url::fromUri($this->uri, (array) $this->options);
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Treat the values as property value of the main property, if no array is
    // given.
    if (isset($values) && !is_array($values)) {
      $values = [static::mainPropertyName() => $values];
    }
    if (isset($values)) {
      $values += [
        'options' => [],
      ];
    }
    // Unserialize the values, this is deprecated as the storage takes care of
    // this, options must not be passed as a string anymore.
    if (is_string($values['options'])) {
      @trigger_error('Support for passing options as a serialized string is deprecated in 8.7.0 and will be removed before Drupal 9.0.0. Pass them as an array instead. See https://www.drupal.org/node/2961643.', E_USER_DEPRECATED);
      $values['options'] = unserialize($values['options'], ['allowed_classes' => FALSE]);
    }
    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints = parent::getConstraints();

    $settings = $this->getSettings();
    $label = $this->getFieldDefinition()->getLabel();

      $uri = $settings['uri_length'];
      $constraints[] = $constraint_manager->create('ComplexData', [
        'uri' => [
          'Length' => [
            'max' => $uri,
            'minMessage' => t('%name: may not be longer than %uri.', ['%name' => $label, '%uri' => $uri]),
          ],
        ],
      ]);
  

      $title = $settings['title_length'];
      $constraints[] = $constraint_manager->create('ComplexData', [
        'title' => [
          'Length' => [
            'max' => $title,
            'maxMessage' => t('%name: may not be longer than %max.', ['%name' => $label, '%title' => $title]),
          ],
        ],
      ]);
  

    return $constraints;
  }

}
