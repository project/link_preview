<?php

namespace Drupal\link_preview\Plugin\Field\FieldWidget;

use Drupal\Core\Url;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Drupal\Component\Utility\NestedArray;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Plugin implementation of the 'link preview' widget.
 *
 * @FieldWidget(
 *   id = "link_preview_default",
 *   label = @Translation("Link Preview"),
 *   field_types = {
 *     "link_preview"
 *   }
 * )
 */
class LinkPreviewWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'placeholder_url' => '',
      'placeholder_title' => '',
      'placeholder_text' => '',      
      'placeholder_rows' => '5',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\link_preview\LinkPreviewItemInterface $item */
    $item = $items[$delta];

    $element['container'] = [
      '#type' => 'container',
      '#title' => $this->t('Link'),
      '#tree' => TRUE,
      '#attributes' => ['id' => 'content-wrapper-'. $delta],
      //'#prefix' => '<div id="content-wrapper-' . $delta . '">',
    ];

    $element['container']['uri'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#placeholder' => $this->getSetting('placeholder_url'),
      // The current field value could have been entered by a different user.
      // However, if it is inaccessible to the current user, do not display it
      // to them.
      '#default_value' => (!$item->isEmpty() && (\Drupal::currentUser()->hasPermission('link to any page') || $item->getUrl()->access())) ? static::getUriAsDisplayableString($item->uri) : NULL,
      '#element_validate' => [[get_called_class(), 'validateUriElement']],
      '#maxlength' => 2048,
      '#required' => $element['#required'],
      '#link_type' => $this->getFieldSetting('link_type'),
      '#ajax' => [
        'callback' => [$this, 'fetchLinkData'], 
        'disable-refocus' => FALSE, 
        'event' => 'change',
        'wrapper' => 'content-wrapper-' . $delta, 
        'progress' => [
          'type' => 'throbber',
          'message' => t('Getting link info...'),
        ],
      ]
    ];
    
    $element['container']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#placeholder' => $this->getSetting('placeholder_title'),
      '#default_value' => isset($items[$delta]->title) ? $items[$delta]->title : NULL,
      '#maxlength' => 255,
      '#required' => FALSE,
    ];

    $element['container']['uri_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Content'),
      '#base_type' => 'textarea',
      '#default_value' => isset($items[$delta]->uri_content) ? $items[$delta]->uri_content : NULL,
      '#rows' => $this->getSetting('placeholder_rows'),
      '#format' => 'full_html',
      '#placeholder' => $this->getSetting('placeholder_text'),
      //'#attributes' => ['class' => ['js-text-full', 'text-full']],
    ];

    $element['#element_validate'][] = [get_called_class(), 'validateTitleElement'];
    $element['#element_validate'][] = [get_called_class(), 'validateTitleNoLink'];

    if (!$element['container']['title']['#required']) {
      // Make title required on the front-end when URI filled-in.
      $field_name = $this->fieldDefinition->getName();

      $parents = $element['#field_parents'];
      $parents[] = $field_name;
      $selector = $root = array_shift($parents);
      if ($parents) {
        $selector = $root . '[' . implode('][', $parents) . ']';
      }

      $element['container']['title']['#states']['required'] = [
        ':input[name="' . $selector . '[' . $delta . '][uri]"]' => ['filled' => TRUE],
      ];
    }

    // Exposing the attributes array in the widget is left for alternate and more
    // advanced field widgets.
    $element['container']['attributes'] = [
      '#type' => 'value',
      '#tree' => TRUE,
      '#value' => !empty($items[$delta]->options['attributes']) ? $items[$delta]->options['attributes'] : [],
      '#attributes' => ['class' => ['link-field-widget-attributes']],
    ];

    // If cardinality is 1, ensure a proper label is output for the field.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      // If the link title is disabled, use the field definition label as the
      // title of the 'uri' element.
      if ($this->getFieldSetting('title') == DRUPAL_DISABLED) {
        $element['container']['uri']['#title'] = $element['container']['#title'];
        // By default the field description is added to the title field. Since
        // the title field is disabled, we add the description, if given, to the
        // uri element instead.
        if (!empty($element['#description'])) {
          if (empty($element['container']['uri']['#description'])) {
            $element['container']['uri']['#description'] = $element['#description'];
          }
          else {
            // If we have the description of the type of field together with
            // the user provided description, we want to make a distinction
            // between "core help text" and "user entered help text". To make
            // this distinction more clear, we put them in an unordered list.
            $element['container']['uri']['#description'] = [
              '#theme' => 'item_list',
              '#items' => [
                // Assume the user-specified description has the most relevance,
                // so place it first.
                $element['#description'],
                $element['container']['uri']['#description'],
              ],
            ];
          }
        }
      }
      // Otherwise wrap everything in a details element.
      else {
        $element += [
          '#type' => 'fieldset',
        ];
      }
    }

    return $element;
  }

  // Ajax Callback where we get link info
function fetchLinkData(array &$form, FormStateInterface $form_state) {
  

   $triggering_element = $form_state->getTriggeringElement();
   // This callback is either invoked from the remove button or the update
   // button, which have different nesting levels.
   $length = end($triggering_element['#parents']) === 'remove_button' ? -4 : -1;
   if (count($triggering_element['#array_parents']) < abs($length)) {
    throw new \LogicException('The element that triggered the widget update was at an unexpected depth. Triggering element parents were: ' . implode(',', $triggering_element['#array_parents']));
   }
   $parents = array_slice($triggering_element['#array_parents'], 0, $length);
   $element = NestedArray::getValue($form, $parents);

   $data = $this->getUriContent($element["uri"]["#value"]);

   $element["title"]["#value"] = $data["title"];
   $element["uri_content"]["value"]["#value"] = $data["body"];

  return $element;
}

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['placeholder_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder for URL'),
      '#default_value' => $this->getSetting('placeholder_url'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];
    $elements['placeholder_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder for link text'),
      '#default_value' => $this->getSetting('placeholder_title'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
      '#states' => [
        'invisible' => [
          ':input[name="instance[settings][title]"]' => ['value' => DRUPAL_DISABLED],
        ],
      ],
    ];
    $element['placeholder_rows'] = [
      '#type' => 'number',
      '#title' => t('Rows for textfield'),
      '#default_value' => $this->getSetting('placeholder_rows'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $element['placeholder_text'] = [
      '#type' => 'textfield',
      '#title' => t('Placeholder for text'),
      '#default_value' => $this->getSetting('placeholder_text'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $placeholder_title = $this->getSetting('placeholder_title');
    $placeholder_url = $this->getSetting('placeholder_url');
    if (empty($placeholder_title) && empty($placeholder_url)) {
      $summary[] = $this->t('No placeholders');
    }
    else {
      if (!empty($placeholder_title)) {
        $summary[] = $this->t('Title placeholder: @placeholder_title', ['@placeholder_title' => $placeholder_title]);
      }
      if (!empty($placeholder_url)) {
        $summary[] = $this->t('URL placeholder: @placeholder_url', ['@placeholder_url' => $placeholder_url]);
      }
    }
    $summary[] = t('Number of rows: @rows', ['@rows' => $this->getSetting('placeholder_rows')]);
    $placeholder = $this->getSetting('placeholder_text');
    if (!empty($placeholder)) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $placeholder]);
    }

    return $summary;
  }
  
  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value['uri'] = static::getUserEnteredStringAsUri($value['container']['uri']);
      $value['title'] = $value['container']['title'];
      $value['uri_content'] = $value['container']['uri_content']['value'];
      $value += ['options' => []];
    }
    return $values; 
  }

  /**
   * Gets the URI without the 'internal:' or 'entity:' scheme.
   *
   * The following two forms of URIs are transformed:
   * - 'entity:' URIs: to entity autocomplete ("label (entity id)") strings;
   * - 'internal:' URIs: the scheme is stripped.
   *
   * This method is the inverse of ::getUserEnteredStringAsUri().
   *
   * @param string $uri
   *   The URI to get the displayable string for.
   *
   * @return string
   *
   * @see static::getUserEnteredStringAsUri()
   */
  protected static function getUriAsDisplayableString($uri) {
    $scheme = parse_url($uri, PHP_URL_SCHEME);

    // By default, the displayable string is the URI.
    $displayable_string = $uri;

    // A different displayable string may be chosen in case of the 'internal:'
    // or 'entity:' built-in schemes.
    if ($scheme === 'internal') {
      $uri_reference = explode(':', $uri, 2)[1];

      // @todo '<front>' is valid input for BC reasons, may be removed by
      //   https://www.drupal.org/node/2421941
      $path = parse_url($uri, PHP_URL_PATH);
      if ($path === '/') {
        $uri_reference = '<front>' . substr($uri_reference, 1);
      }

      $displayable_string = $uri_reference;
    }
    elseif ($scheme === 'entity') {
      list($entity_type, $entity_id) = explode('/', substr($uri, 7), 2);
      // Show the 'entity:' URI as the entity autocomplete would.
      // @todo Support entity types other than 'node'. Will be fixed in
      //    https://www.drupal.org/node/2423093.
      if ($entity_type == 'node' && $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id)) {
        $displayable_string = EntityAutocomplete::getEntityLabels([$entity]);
      }
    }
    elseif ($scheme === 'route') {
      $displayable_string = ltrim($displayable_string, 'route:');
    }

    return $displayable_string;
  }

  /**
   * Gets the user-entered string as a URI.
   *
   * The following two forms of input are mapped to URIs:
   * - entity autocomplete ("label (entity id)") strings: to 'entity:' URIs;
   * - strings without a detectable scheme: to 'internal:' URIs.
   *
   * This method is the inverse of ::getUriAsDisplayableString().
   *
   * @param string $string
   *   The user-entered string.
   *
   * @return string
   *   The URI, if a non-empty $uri was passed.
   *
   * @see static::getUriAsDisplayableString()
   */
  protected static function getUserEnteredStringAsUri($string) {
    // By default, assume the entered string is an URI.
    $uri = trim($string);

    // Detect entity autocomplete string, map to 'entity:' URI.
    $entity_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($string);
    if ($entity_id !== NULL) {
      // @todo Support entity types other than 'node'. Will be fixed in
      //    https://www.drupal.org/node/2423093.
      $uri = 'entity:node/' . $entity_id;
    }
    // Support linking to nothing.
    elseif (in_array($string, ['<nolink>', '<none>'], TRUE)) {
      $uri = 'route:' . $string;
    }
    // Detect a schemeless string, map to 'internal:' URI.
    elseif (!empty($string) && parse_url($string, PHP_URL_SCHEME) === NULL) {
      // @todo '<front>' is valid input for BC reasons, may be removed by
      //   https://www.drupal.org/node/2421941
      // - '<front>' -> '/'
      // - '<front>#foo' -> '/#foo'
      if (strpos($string, '<front>') === 0) {
        $string = '/' . substr($string, strlen('<front>'));
      }
      $uri = 'internal:' . $string;
    }

    return $uri;
  }



  /**
   * Uses URI to fetch content from it, using goute library
   *
   * @param string $uri
   *   The URI to get the content for.
   *
   * @return array $content
   *   Title and content of uri
   */
  private function getUriContent($uri) {
    $client = new Client();

    $crawler = $client->request('GET', $uri);
    $title = trim($crawler->filterXPath('//body//h1')->text());
    $children = $crawler->filter('body')->children();

    $current_biggest = 1;
    $blocked_tags = array("script","iframe", "style", "noscript", "img");
    foreach ($children as $child){
      if (strlen($child->nodeValue) > $current_biggest && !in_array($child->nodeName, $blocked_tags)) {
        $bigest_node = $child->getNodePath();
        $current_biggest = strlen($child->nodeValue);
      }
    }

    // If first h1 is empty, try to use one with bigest node if it is there
    if (empty($title)){
      $title = $crawler->filterXPath("/" . $bigest_node . "//h1")->text();
    }

    // Find highest number of paragrapsh in bulk
    $paragraphs = $crawler->filterXPath("/" . $bigest_node . "//p");

    $highest_paragraphs_no = 1;
    foreach ($paragraphs as $domElement) {
          if ($domElement->parentNode->childNodes->count() > $highest_paragraphs_no) {
              $highest_paragraphs_no = $domElement->parentNode->childNodes->count();
              $highest_paragraphs_element = $domElement->parentNode->getNodePath();
          }
    }

    $body = $crawler->filterXPath("/" . $highest_paragraphs_element)->children()->each(function ($node) {
      // Which tags do we want to extract, 
      // TODO make this configurable
      $permited_tags = array("p","h2","h3","h4",);
      if (in_array($node->nodeName(), $permited_tags)) {
          if (!empty(trim($node->text()))){
            return '<' .$node->nodeName() . '>' . $node->text() . '</' . $node->nodeName() . '>';
          }
        }  
    });

    $content = array("title" => $title, "body" => implode("", $body));

    return $content;  
  }

  /**
   * Form element validation handler for the 'uri' element.
   *
   * Disallows saving inaccessible or untrusted URLs.
   */
  public static function validateUriElement($element, FormStateInterface $form_state, $form) {
    $uri = static::getUserEnteredStringAsUri($element['#value']);
    $form_state->setValueForElement($element, $uri);

    // If getUserEnteredStringAsUri() mapped the entered value to a 'internal:'
    // URI , ensure the raw value begins with '/', '?' or '#'.
    // @todo '<front>' is valid input for BC reasons, may be removed by
    //   https://www.drupal.org/node/2421941
    if (parse_url($uri, PHP_URL_SCHEME) === 'internal' && !in_array($element['#value'][0], ['/', '?', '#'], TRUE) && substr($element['#value'], 0, 7) !== '<front>') {
      $form_state->setError($element, t('Manually entered paths should start with one of the following characters: / ? #'));
      return;
    }
  }

  /**
   * Form element validation handler for the 'title' element.
   *
   * Conditionally requires the link title if a URL value was filled in.
   */
  public static function validateTitleElement(&$element, FormStateInterface $form_state, $form) {
    if ($element['container']['uri']['#value'] !== '' && $element['container']['title']['#value'] === '') {
      // We expect the field name placeholder value to be wrapped in t() here,
      // so it won't be escaped again as it's already marked safe.
      $form_state->setError($element['container']['title'], t('@title field is required if there is @uri input.', ['@title' => $element['container']['title']['#title'], '@uri' => $element['container']['uri']['#title']]));
    }
  }

  /**
   * Form element validation handler for the 'title' element.
   *
   * Requires the URL value if a link title was filled in.
   */
  public static function validateTitleNoLink(&$element, FormStateInterface $form_state, $form) {
    if ($element['container']['uri']['#value'] === '' && $element['container']['title']['#value'] !== '') {
      $form_state->setError($element['container']['uri'], t('The @uri field is required when the @title field is specified.', ['@title' => $element['container']['title']['#title'], '@uri' => $element['container']['uri']['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   *
   * Override the '%uri' message parameter, to ensure that 'internal:' URIs
   * show a validation error message that doesn't mention that scheme.
   */
  public function flagErrors(FieldItemListInterface $items, ConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    /** @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
    foreach ($violations as $offset => $violation) {
      $parameters = $violation->getParameters();
      if (isset($parameters['@uri'])) {
        $parameters['@uri'] = static::getUriAsDisplayableString($parameters['@uri']);
        $violations->set($offset, new ConstraintViolation(
          $this->t($violation->getMessageTemplate(), $parameters),
          $violation->getMessageTemplate(),
          $parameters,
          $violation->getRoot(),
          $violation->getPropertyPath(),
          $violation->getInvalidValue(),
          $violation->getPlural(),
          $violation->getCode()
        ));
      }
    }
    parent::flagErrors($items, $violations, $form, $form_state);
  }

}
