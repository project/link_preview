<?php

namespace Drupal\link_preview\Plugin\Validation\Constraint;

use Drupal\link_preview\LinkPreviewItemInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Constraint validator for links receiving data allowed by its settings.
 */
class LinkTypeConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    if (isset($value)) {
      $uri_is_valid = TRUE;

      /** @var $link_item \Drupal\link_preview\LinkPreviewItemInterface */
      $link_item = $value;
      $link_type = $link_item->getFieldDefinition()->getSetting('link_type');

      // Try to resolve the given URI to a URL. It may fail if it's schemeless.
      try {
        $url = $link_item->getUrl();
      }
      catch (\InvalidArgumentException $e) {
        $uri_is_valid = FALSE;
      }

      if (!$uri_is_valid) {
        $this->context->addViolation($constraint->message, ['@uri' => $link_item->uri]);
      }
    }
  }

}
